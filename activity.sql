USE blog_db;

SHOW TABLES;

DESCRIBE users;

INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", '2021-01-01 1:00:00'),
("juandelacruz@gmail.com", "passwordB",  '2021-01-01 2:00:00'),
("janesmith@gmail.com", "passwordC",  '2021-01-01 3:00:00'),
("mariadelacruz@gmail.com", "passwordD",  '2021-01-01 4:00:00'),
("johndoe@gmail.com", "passwordE",  '2021-01-01 5:00:00');



INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES ( 1, "First Code", "Hello World!", '2021-01-02 1:00:00'),
( 1, "Second Code", "Hello Earth!", '2021-01-02 2:00:00'),
( 2, "Third Code", "Welcome to Mars!", '2021-01-02 3:00:00'),
( 4, "Fourth Code", "Bye bye solar system!", '2021-01-02 4:00:00');


SELECT * 
FROM  posts 
WHERE author_id = 1;

SELECT email, datetime_created
FROM users;

UPDATE posts
SET content = "Hello to the People of the Earth"
WHERE id = 2;

DELETE FROM users
WHERE email = "johndoe@gmail.com";